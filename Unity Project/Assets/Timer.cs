﻿using UnityEngine;
using System.Collections;
using System;

public class Timer : MonoBehaviour

{
    public event EventHandler<TimerColEventArgs> OnTimerCol;
    [SerializeField] private float cooldownTime;
    private float timeStart;
    private float timeEnd;


    // Use this for initialization
    void Start () 
    {
        timeStart = Time.time;
	}

    // Update is called once per frame
    private void Update()
    {
        timeEnd = Time.time - timeStart;

        if (cooldownTime <= timeEnd)
        {
            timeStart = Time.time;
            if (OnTimerCol != null)
            {
                OnTimerCol(this, new TimerColEventArgs(this));
            }

        }
    }
    #region Timer

    public class TimerColEventArgs : EventArgs
    {
        public Timer SpownBubble { get; private set; }

        public TimerColEventArgs(Timer spownObject)
        {
            SpownBubble = spownObject;
        }
    }

    #endregion
}
