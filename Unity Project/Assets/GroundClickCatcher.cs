﻿using UnityEngine;
using System.Collections;

public class GroundClickCatcher : ClickCatcher
{
    public override void ClickCatch(RaycastHit hit)
    {
        var gob = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        gob.transform.position = hit.point;
        gob.GetComponent<Renderer>().material.color = Color.green;
        gob.AddComponent<Rigidbody>();
    }
}
