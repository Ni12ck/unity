﻿using UnityEngine;
using System.Collections;

public class CharCont : MonoBehaviour

{
    [SerializeField] public GameObject _target;
    [SerializeField] public float speed;
    [SerializeField] public float seeDistance;
    [SerializeField] private GameObject AimPrefab;
    private GameObject Aim;

    private void Start()
    {
        CreateAim();
    }
    void Update()
    {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.CompareTag("Ground"))
                {
                    Aim.GetComponent<AimLogic>().AimOn();
                    Aim.transform.position = hit.point;

                }
            }
        }
        if (Vector3.Distance(transform.position, Aim.transform.position) > seeDistance)
        {
            Move();
        }
    }
    public void CreateAim()
    {
        Vector3 AimPosition = new Vector3(0, 0, 0);
        Aim = Instantiate(AimPrefab, AimPosition, Quaternion.LookRotation(Vector3.down)) as GameObject;
        Aim.SetActive(false);
    }
    private void Move()
    {

        transform.LookAt(Aim.transform);
        transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));

    }
}
//{
//public var play:boolean = true; 
//public var speed: int = 10; 
//public var moveDirection: Vector3; 
//public var grounded: boolean; 
//public var gravity: int = 1; 

//function Update()
//{
//if (play)
//{
//if (Input.GetKey(KeyCode.Escape))
//{
//Application.Quit();
//}

//if (grounded)
//{
//moveDirection = transform.TransformDirection(Vector3.forward);
//moveDirection *= speed;
//}
//moveDirection.y -= gravity * Time.deltaTime;
//if (Input.GetMouseButton(1))
//{
//var dx: int = Input.mousePosition.x - Screen.width / 2.0;
//var dy: int = Input.mousePosition.y - Screen.height / 2.0;
//var strawRadians: float = Mathf.Atan2(dx, dy);
//var strawDigrees:float = 360.0 * strawRadians / (2.0 * Mathf.PI);
//transform.rotation.eulerAngles.y = strawDigrees;
//var controller : CharacterController = GetComponent(CharacterController);
//var flags = controller.Move(moveDirection * Time.deltaTime);
//grounded = (flags & CollisionFlags.CollidedBelow) != 0;
//}
//}
//}

//public float speed = 6.0F;
//public float jumpSpeed = 8.0F;
//public float gravity = 20.0F;
//private Vector3 moveDirection = Vector3.zero;
//[SerializeField] private CharacterController CharCont;
//[SerializeField] private Vector3 Positoin;
//private Vector3 PositoinModel = Vector3.zero;
//private Vector3 TargetModel;
//public GameObject model;

//private void Start ()
//{
//CharacterController conroller = GetComponent<CharacterController>();
//}

//void Update()
//{
//Ray ray = Camera.ScreenPointToRay(Input.mousePosition);
//RaycastHit hit = new RaycastHit();
//if (Physics.Raycast(ray, out hit))
//{
//Vector3 rot = transform.eulerAngles;
//transform.LookAt(hit.point);
//transform.eulerAngles = new Vector3(rot.x, transform.eulerAngles.y - 90, rot.z);
//}
//}
//CharacterController controller = GetComponent<CharacterController>();

//if (controller.isGrounded)
//{
//moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
//moveDirection = transform.TransformDirection(moveDirection);
//moveDirection *= speed;
//if (Input.GetButton("Jump"))
//moveDirection.y = jumpSpeed;
//}
//moveDirection.y -= gravity * Time.deltaTime;
//controller.Move(moveDirection * Time.deltaTime);


//}
