﻿using UnityEngine;
using System.Collections;

public class BubbleClickCatcher : ClickCatcher
{
    public override void ClickCatch(RaycastHit hit)
        {
            GetComponent<Renderer>().material.color = Color.yellow;
        }
}
