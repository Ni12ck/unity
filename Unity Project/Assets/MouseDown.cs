﻿using UnityEngine;
using System.Collections;

public class MouseDown : MonoBehaviour
{
	private RaycastHit hit;

	void Start()
    {
    }
	

	void Update()
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hit))
			{
				if (hit.collider.CompareTag ("Ground")) 
				{
					GameObject gob = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
					gob.transform.position = hit.point;
				}
				if (hit.collider.CompareTag ("RainBubble")) 
				{
					Destroy(hit.collider.gameObject);
				}
			}
		}

		if (Input.GetMouseButtonDown (1)) 
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast (ray, out hit))
			{
				if (hit.collider.CompareTag ("RainBubble")) 
				{
					hit.collider.gameObject.GetComponent<Renderer> ().material.color = Color.red;
			   }
	       }
       }
	}
}