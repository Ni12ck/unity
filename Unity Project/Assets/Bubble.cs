﻿using UnityEngine;
using System;
using System.Collections;

public class Bubble : MonoBehaviour 

{
    public event EventHandler<FallToAbyssEventArgs> OnFallToAbyss;

    private Renderer myRenderer;

    private void Start()
    {
        myRenderer = GetComponent<Renderer>();
    }
     private void OnCollisionEnter(Collision collision)
    {
        {
            if (collision.gameObject.CompareTag("Ground"))
            {
                myRenderer.material.color = Color.blue;
            }
            
        }
        
        
    }

    private void Update()
    {
        if (transform.position.y <= 0)
        {
            if (OnFallToAbyss != null)
            {
                OnFallToAbyss(this, new FallToAbyssEventArgs(this));
            }

        }
    }

    #region Event args

    public class FallToAbyssEventArgs: EventArgs
    {
        public Bubble FalledBubble { get; private set; }

        public FallToAbyssEventArgs(Bubble falledBubble)
        {
            FalledBubble = falledBubble;
        }
    }

    #endregion
}